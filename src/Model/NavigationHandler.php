<?php

namespace App\Model;


use App\Repository\CategoryRepository;
use App\Repository\TagsRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NavigationHandler
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var TagsRepository
     */
    private $tagsRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        TagsRepository $tagsRepository,
        ContainerInterface $container

    )
    {
        $this->container = $container;
        $this->categoryRepository = $categoryRepository;
        $this->tagsRepository = $tagsRepository;
    }

    /**
     * @return array
     */
    public function getNewsCategoriesLinks(): array
    {
        $categories = $this->categoryRepository->findAll();
        $links = [];
        foreach ($categories as $category) {
            $links[] = [
                "id" => $category->getId(),
                "title" => $category->getName(),
            ];
        }
        return $links;
    }

    public function getNewsTagsLinks(): array
    {
        $categories = $this->tagsRepository->findAll();
        $links = [];
        foreach ($categories as $category) {
            $links[] = [
                "id" => $category->getId(),
                "title" => $category->getName(),
            ];
        }
        return $links;
    }

}