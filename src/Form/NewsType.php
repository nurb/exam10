<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Tags;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('publishDate', DateType::class, array(
                'label' => false,
                'years' => range(date('Y'), date('Y') + 5),
                'months' => range(date('m'), 12),
                'days' => range(date('d'), 31),
                'widget' => 'choice',
            ))
            ->add('content', TextareaType::class, array(
                'label' => "Содержание(контент)",
                'attr' => array(
                    'placeholder' => 'Ваша новость',
                ),
            ))
            ->add('category', EntityType::class, array(
                'class' => Category::class,
                'placeholder' => 'Выберите категорию',
                'choice_label' => 'name',

            ))
            ->add('tags', EntityType::class, [
                'class' => Tags::class,
                'required' => false,
                'choice_label' => 'name',
                'multiple' => true,
                'by_reference' => false
            ])
            ->add('save', SubmitType::class,
                array('label' => 'Добавить пост',
                    'attr' => ["class" => "btn btn-success"]
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_news_type';
    }
}
