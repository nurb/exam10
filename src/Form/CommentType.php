<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('post', TextType::class, array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'Комментарий',
            ),
        ))
            ->add('save', SubmitType::class,
                array('label' => 'Комментировать',
                    'attr' => ["class" => "btn btn-success"]
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_comment_type';
    }
}
