<?php

namespace App\DataFixtures;

use App\Entity\Category;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoriesFixtures extends Fixture
{
    public const CAT_ONE = 'Мир';
    public const CAT_TWO = 'Экономика';
    public const CAT_THREE = 'Спорт';
    public const CAT_FOUR = 'Культура';

    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1
            ->setName('Мир');
        $manager->persist($category1);

        $category2 = new Category();
        $category2
            ->setName('Экономика');
        $manager->persist($category2);

        $category3 = new Category();
        $category3
            ->setName('Спорт');

        $manager->persist($category3);

        $category4 = new Category();
        $category4
            ->setName('Культура');

        $manager->persist($category4);


        $this->addReference(self::CAT_ONE, $category1);
        $this->addReference(self::CAT_TWO, $category2);
        $this->addReference(self::CAT_THREE, $category3);
        $this->addReference(self::CAT_FOUR, $category4);
        $manager->flush();

    }

}
