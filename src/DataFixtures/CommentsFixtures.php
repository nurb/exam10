<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CommentsFixtures extends Fixture implements DependentFixtureInterface
{
    public const COMMENT_ONE = 'Спасибо, отличные новости';
    public const COMMENT_TWO = 'Ужас';
    public const COMMENT_THREE = 'Мне нравится';
    public const COMMENT_FOUR = 'Ух, ты';

    private function getRandomUser()
    {
        $users = [$this->getReference(UserFixtures::USER_RICK),
            $this->getReference(UserFixtures::USER_MORTY),
            $this->getReference(UserFixtures::USER_ADMIN)];
        return $users[array_rand($users)];
    }

    private function getRandomPost()
    {
        $post = [self::COMMENT_ONE,
            self::COMMENT_TWO,
            self::COMMENT_THREE,
            self::COMMENT_FOUR];
        return $post[array_rand($post)];
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < NewsFixtures::NEWS_COUNT; $i++) {
            $comment1 = new Comment();
            $comment1
                ->setPost($this->getRandomPost())
                ->setAuthor($this->getRandomUser())
                ->setNews($this->getReference('news-'.$i));
            $manager->persist($comment1);

            $comment2 = new Comment();
            $comment2
                ->setPost($this->getRandomPost())
                ->setAuthor($this->getRandomUser())
                ->setNews($this->getReference('news-'.$i));
            $manager->persist($comment2);

            $comment3 = new Comment();
            $comment3
                ->setPost($this->getRandomPost())
                ->setAuthor($this->getRandomUser())
                ->setNews($this->getReference('news-'.$i));
            $manager->persist($comment3);

            $comment4 = new Comment();
            $comment4
                ->setPost($this->getRandomPost())
                ->setAuthor($this->getRandomUser())
                ->setNews($this->getReference('news-'.$i));
            $manager->persist($comment4);
        }
        $manager->flush();

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return array(
            UserFixtures::class,
            NewsFixtures::class
        );
    }
}