<?php

namespace App\DataFixtures;

use App\Entity\Relevance;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RelevanceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $users1 = $this->getReference(UserFixtures::USER_RICK);
        $users2 = $this->getReference(UserFixtures::USER_MORTY);
        $users3 = $this->getReference(UserFixtures::USER_ADMIN);

        for ($i = 0; $i < NewsFixtures::NEWS_COUNT; $i++) {
            $postLike1 = new Relevance();
            $postLike1
                ->setUser($users1)
                ->setNews($this->getReference('news-' . $i))
                ->setIsGood($this->getDesision());
            $manager->persist($postLike1);

            $postLike2 = new Relevance();
            $postLike2
                ->setUser($users2)
                ->setNews($this->getReference('news-' . $i))
                ->setIsGood($this->getDesision());
            $manager->persist($postLike2);

            $postLike3 = new Relevance();
            $postLike3
                ->setUser($users3)
                ->setNews($this->getReference('news-' . $i))
                ->setIsGood($this->getDesision());
            $manager->persist($postLike3);


        }
        $manager->flush();

    }

    private function getDesision()
    {
        if ((bool)rand(0, 1)) {
            return 1;
        } else {
            return -1;
        }

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return array(
            UserFixtures::class,
            NewsFixtures::class
        );
    }
}