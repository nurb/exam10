<?php

namespace App\DataFixtures;

use App\Entity\Tags;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;



class TagsFixtures extends Fixture
{
    public const TAG_ONE = 'добро';
    public const TAG_TWO = 'люди';
    public const TAG_THREE = 'жизнь';
    public const TAG_FOUR = 'тэг';

    public function load(ObjectManager $manager)
    {
        $tag1 = new Tags();
        $tag1
            ->setName('добро');
        $manager->persist($tag1);

        $tag2 = new Tags();
        $tag2
            ->setName('люди');
        $manager->persist($tag2);

        $tag3 = new Tags();
        $tag3
            ->setName('жизнь');

        $manager->persist($tag3);

        $tag4 = new Tags();
        $tag4
            ->setName('тэг');

        $manager->persist($tag4);


        $this->addReference(self::TAG_ONE, $tag1);
        $this->addReference(self::TAG_TWO, $tag2);
        $this->addReference(self::TAG_THREE, $tag3);
        $this->addReference(self::TAG_FOUR, $tag4);
        $manager->flush();

    }

}
