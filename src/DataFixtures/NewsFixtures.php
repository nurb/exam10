<?php

namespace App\DataFixtures;

use App\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class NewsFixtures extends Fixture implements DependentFixtureInterface
{

    public const NEWS_COUNT = 100;

    private function getRandomUser()
    {
        $users = [$this->getReference(UserFixtures::USER_RICK),
            $this->getReference(UserFixtures::USER_MORTY),
            $this->getReference(UserFixtures::USER_ADMIN)];
        return $users[array_rand($users)];
    }

    private function getRandomCategory()
    {
        $category = [$this->getReference(CategoriesFixtures::CAT_ONE),
            $this->getReference(CategoriesFixtures::CAT_TWO),
            $this->getReference(CategoriesFixtures::CAT_THREE),
            $this->getReference(CategoriesFixtures::CAT_FOUR)];
        return $category[array_rand($category)];
    }

    private function getRandomTags()
    {
        $tag = [$this->getReference(TagsFixtures::TAG_ONE),
            $this->getReference(TagsFixtures::TAG_TWO),
            $this->getReference(TagsFixtures::TAG_THREE),
            $this->getReference(TagsFixtures::TAG_FOUR)];
        return $tag[array_rand($tag)];
    }

    function getRandomDate(): \DateTime
    {
        $rand_epoch = rand(1535760000, 1543622400);
        return new \DateTime(date('Y-m-d H:i:s', $rand_epoch));
    }


    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::NEWS_COUNT; $i++) {
            try {
                $content = file_get_contents('http://loripsum.net/api/7/medium/decorate/link/ul/');
            } catch (\Exception $e) {
                $content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Accusantium aliquid assumenda at, cum cupiditate debitis deleniti dicta 
            eius error eum id inventore ipsa itaque magni modi nemo, nostrum odit porro provident 
            quas quis quo recusandae repellat repellendus sequi sint sit sunt suscipit ullam, vero voluptas voluptate 
            voluptatem voluptatibus. Amet aperiam facilis quae quia, repellat suscipit voluptates.
            Alias assumenda esse ipsum, laboriosam nisi nulla obcaecati pariatur possimus, quidem quisquam quod saepe 
            similique suscipit voluptate voluptates? Adipisci aliquam, aperiam debitis dignissimos excepturi hic iure, 
            laboriosam magnam quae qui reprehenderit temporibus, vitae voluptas? Cumque fuga fugiat iste iusto 
            nam ratione voluptatibus. Quasi, tenetur.';
            }
            $tagOne = $this->getRandomTags();
            $tagTwo = $tagOne;
            while ($tagTwo == $tagOne) {
                $tagTwo = $this->getRandomTags();
            }


            $news = new News();
            $news
                ->setContent($content)
                ->setPublishDate($this->getRandomDate())
                ->setAuthor($this->getRandomUser())
                ->setCategory($this->getRandomCategory())
                ->addTags($tagOne)
                ->addTags($tagTwo);
            $manager->persist($news);
            $this->addReference('news-' . $i, $news);
            $manager->flush();
        }
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return array(
            CategoriesFixtures::class,
            TagsFixtures::class,
            UserFixtures::class,
        );
    }

}
