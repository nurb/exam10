<?php

namespace App\Controller;


use App\Entity\Comment;
use App\Entity\News;
use App\Entity\PostLike;
use App\Entity\Quality;
use App\Entity\Relevance;
use App\Form\CommentType;
use App\Form\NewsType;
use App\Repository\LikeRepository;
use App\Repository\NewsRepository;
use App\Repository\QualityRepository;
use App\Repository\RelevanceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends Controller
{
    /**
     * @Route("/news/{id}", requirements={"id": "\d+"}, name="app_single_news")
     * @param NewsRepository $newsRepository
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function singleNewsAction(NewsRepository $newsRepository, int $id)
    {
        $comment = new Comment();
        $comment_form = $this->createForm(CommentType::class, $comment, array(
            'method' => 'POST'
        ));
        $singleNews = $newsRepository->find($id);
        return $this->render('single_news.html.twig', [
            'single_news' => $singleNews,
            'comment_form' => $comment_form
        ]);
    }

    /**
     * @Route("/news/create", name="app_create_news")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createNewsAction(Request $request)
    {
        if ($this->getUser()) {
            $newPost = new News();
            $form = $this->createForm(NewsType::class, $newPost, array(
                'method' => 'POST'
            ));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $newPost->setAuthor($this->getUser());
                if (!in_array("ROLE_ADMIN", $this->getUser()->getRoles())) {
                    $newPost->setPublishDate(null);
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($newPost);
                $em->flush();

                return $this->redirectToRoute("homepage");
            }
            $showForm = $form->createView();
        } else {
            $showForm = null;
        }

        return $this->render('create_news.html.twig', array(
            "form" => $showForm,
        ));
    }

    /**
     * @Route("/news/filter/{type}/{id}", name="app_filter_news")
     * @param NewsRepository $newsRepository
     * @param string $type
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function filterNewsAction(NewsRepository $newsRepository,
                                     string $type,
                                     int $id, Request $request)
    {
        /**
         * @var $news News
         */
        if ($type == 'category') {
            $news = $newsRepository->getNewsByCategory($id);
        } elseif ($type == 'tag') {
            $news = $newsRepository->getNewsByTags($id);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        return $this->render('index.html.twig', [
            'news' => $pagination
        ]);
    }


    /**
     * @Route("/news/rate/{id}/{type}/{rating}", name="app_rate_post")
     * @param int $rating
     * @param string $type
     * @param int $id
     * @param Request $request
     * @param ObjectManager $objectManager
     * @param NewsRepository $newsRepository
     * @param LikeRepository $likeRepository
     * @param QualityRepository $qualityRepository
     * @param RelevanceRepository $relevanceRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ratePostAction(int $rating,
                                   string $type,
                                   int $id,
                                   Request $request,
                                   ObjectManager $objectManager,
                                   NewsRepository $newsRepository,
                                   LikeRepository $likeRepository,
                                   QualityRepository $qualityRepository,
                                   RelevanceRepository $relevanceRepository
    )
    {


        $news = $newsRepository->find($id);
        if ($this->getUser()) {
            if ($type == "like") {
                if ($likeRepository->checkDuplicate($this->getUser(), $news) == null) {
                    $like = new PostLike();
                    $like->setNews($news);
                    $like->setIsGood($rating);
                    $like->setUser($this->getUser());
                    $objectManager->persist($like);
                    $this->addFlash('notice', "Спасибо, что проголосовали");
                } else {
                    $this->addFlash('error', 'Вы уже голосавали');
                }
            }

            if ($type == "relevance") {
                if ($relevanceRepository->checkDuplicate($this->getUser(), $news) == null) {
                    $like = new Relevance();
                    $like->setNews($news);
                    $like->setIsGood($rating);
                    $like->setUser($this->getUser());
                    $objectManager->persist($like);
                    $this->addFlash('notice', "Спасибо, что проголосовали");
                } else {
                    $this->addFlash('error', 'Вы уже голосавали');
                }
            }

            if ($type == "quality") {
                if ($qualityRepository->checkDuplicate($this->getUser(), $news) == null) {
                    $like = new Quality();
                    $like->setNews($news);
                    $like->setIsGood($rating);
                    $like->setUser($this->getUser());
                    $objectManager->persist($like);
                    $this->addFlash('notice', "Спасибо, что проголосовали");
                } else {
                    $this->addFlash('error', 'Вы уже голосавали');
                }
            }
        }

        $objectManager->flush();
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/news/comment/add/{id}", requirements={"id": "\d+"}, name="app-add-comment")
     * @Method({"GET","HEAD", "POST"})
     * @param Request $request
     * @param int $id
     * @param NewsRepository $newsRepository
     * @param ObjectManager $objectManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addCommentAction(Request $request, int $id, NewsRepository $newsRepository, ObjectManager $objectManager)
    {
        if ($this->getUser()) {
            $comment = new Comment();
            $form = $this->createForm(CommentType::class, $comment, array(
                'method' => 'POST'
            ));
            $form->handleRequest($request);
            $comment->setAuthor($this->getUser());
            $news = $newsRepository->find($id);

            if ($form->isSubmitted() && $form->isValid()) {
                $comment->setNews($news);
                $objectManager->persist($comment);
                $objectManager->flush();
            }
        }


        return $this->redirect($request->headers->get('referer'));
    }


}