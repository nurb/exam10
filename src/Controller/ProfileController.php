<?php

namespace App\Controller;


use App\Repository\NewsRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends Controller
{
    /**
     * @Route("/profile/{id}", name="app_show_profile")
     * @param UserRepository $userRepository
     * @param NewsRepository $newsRepository
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showProfileAction(UserRepository $userRepository, NewsRepository $newsRepository, int $id)
    {

        $user = $userRepository->find($id);
        $rating = 0;
        if ($user && $newsRepository->countUserNews($user)) {
            $rating = array_sum($newsRepository->countUserNews($user)) / 3;
        }

        return $this->render('profile.html.twig', [
            'user' => $user,
            'rating' => $rating

        ]);
    }
}