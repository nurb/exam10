<?php

namespace App\Controller;

use App\Model\NavigationHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class NavigationController extends Controller
{
    /**
     * @Route("/sidebar-navigation", name="app_navigation_sidebar")
     * @param NavigationHandler $navigationHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showSidebarNavigationAction(NavigationHandler $navigationHandler)
    {
        $category_links = $navigationHandler->getNewsCategoriesLinks();
        $tags_links = $navigationHandler->getNewsTagsLinks();

        return $this->render('navigation/sidebar_menu.html.twig', [
            'category_links' => $category_links,
            'tags_links' => $tags_links
        ]);
    }
}