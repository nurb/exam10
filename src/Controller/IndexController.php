<?php


namespace App\Controller;


use App\Repository\NewsRepository;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param NewsRepository $newsRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(NewsRepository $newsRepository, Request $request)
    {
        $news = $newsRepository->getHotNews();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );


        return $this->render('index.html.twig', [
            'news' => $pagination
        ]);
    }

}