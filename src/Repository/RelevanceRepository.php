<?php

namespace App\Repository;

use App\Entity\Relevance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Relevance|null find($id, $lockMode = null, $lockVersion = null)
 * @method Relevance|null findOneBy(array $criteria, array $orderBy = null)
 * @method Relevance[]    findAll()
 * @method Relevance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelevanceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Relevance::class);
    }

    public function checkDuplicate($user, $news)
    {
        try {
            return $this->createQueryBuilder('l')
                ->select('l')
                ->where('l.news = :news')
                ->setParameter('news', $news)
                ->andWhere('l.user = :user')
                ->setParameter('user', $user)
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
        }
    }

//    /**
//     * @return Relevance[] Returns an array of Relevance objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Relevance
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
