<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }


    public function getHotNews()
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.publishDate is NOT NULL')
            ->andWhere('a.publishDate <= CURRENT_DATE()')
            ->orderBy('a.publishDate', 'DESC')
            ->getQuery()
            ->getResult();
    }


    public function getNewsByCategory($category)
    {
        return $this->createQueryBuilder('n')
            ->select('n')
            ->where('n.category = :category')
            ->setParameter('category', $category)
            ->andWhere('n.publishDate is NOT NULL')
            ->andWhere('n.publishDate <= CURRENT_DATE()')
            ->orderBy('n.publishDate', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getNewsByTags($tag)
    {
        return $this->createQueryBuilder('n')
            ->select('n')
            ->innerJoin('n.tags', 't')
            ->where('t.id = :tag')
            ->setParameter('tag', $tag)
            ->andWhere('n.publishDate is NOT NULL')
            ->andWhere('n.publishDate <= CURRENT_DATE()')
            ->orderBy('n.publishDate', 'DESC')
            ->getQuery()
            ->getResult();
    }


    public function countUserNews($user)
    {
        try {
            return $this->createQueryBuilder('n')
                ->select('SUM(q.isGood), SUM(r.isGood), COUNT(n)')
                ->innerJoin('n.qualites', 'q')
                ->innerJoin('n.relevances', 'r')
                ->where('n.author = :user')
                ->setParameter('user', $user)
                ->andWhere('n.publishDate is NOT NULL')
                ->andWhere('n.publishDate <= CURRENT_DATE()')
                ->orderBy('n.publishDate', 'DESC')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }


//    /**
//     * @return News[] Returns an array of News objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?News
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
