<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagsRepository")
 */
class Tags
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\News", mappedBy="tags", cascade={"persist", "remove"})
     */
    private $news;

    public function __construct()
    {
        $this->news = new ArrayCollection();
    }


    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return Tags
     */
    public function setName(string $name): Tags
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param ArrayCollection $news
     * @return Tags
     */
    public function addNews(ArrayCollection $news): Tags
    {
        $this->news->add($news);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getNews(): ArrayCollection
    {
        return $this->news;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
