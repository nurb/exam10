<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QualityRepository")
 */
class Quality
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $isGood;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\News", inversedBy="qualites")
     */
    private $news;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="likes")
     */
    private $user;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $isGood
     * @return Quality
     */
    public function setIsGood(int $isGood): Quality
    {
        $this->isGood = $isGood;
        return $this;
    }

    /**
     * @return int
     */
    public function isGood(): int
    {
        return $this->isGood;
    }

    /**
     * @param mixed $news
     * @return Quality
     */
    public function setNews($news)
    {
        $this->news = $news;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * @param mixed $user
     * @return Quality
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
}
