<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LikeRepository")
 */
class PostLike
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $isGood;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\News", inversedBy="likes")
     */
    private $news;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="likes")
     */
    private $user;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $isGood
     * @return PostLike
     */
    public function setIsGood(int $isGood): PostLike
    {
        $this->isGood = $isGood;
        return $this;
    }

    /**
     * @return int
     */
    public function isGood(): int
    {
        return $this->isGood;
    }

    /**
     * @param mixed $news
     * @return PostLike
     */
    public function setNews($news)
    {
        $this->news = $news;
        return $this;
    }

    /**
     * @param mixed $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }
}
