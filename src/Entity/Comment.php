<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentsRepository")
 */
class Comment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", length=1024)
     * @var string
     */
    private $post;

    /**
     * @var News
     * @ORM\ManyToOne(targetEntity="App\Entity\News", inversedBy="comments")
     */
    private $news;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="comments")
     */
    private $author;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param User $author
     * @return Comment
     */
    public function setAuthor(User $author): Comment
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @param string $post
     * @return Comment
     */
    public function setPost(string $post): Comment
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @return string
     */
    public function getPost():? string
    {
        return $this->post;
    }

    /**
     * @param News $news
     * @return Comment
     */
    public function setNews(News $news): Comment
    {
        $this->news = $news;
        return $this;
    }

    /**
     * @return News
     */
    public function getNews(): News
    {
        return $this->news;
    }

    public function __toString()
    {
        return $this->getPost() . "";
    }
}
