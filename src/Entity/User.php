<?php


// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\News", mappedBy="author", cascade={"persist", "remove"})
     */
    private $news;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="author", cascade={"persist", "remove"})
     */
    private $comments;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Quality", mappedBy="user", cascade={"persist", "remove"})
     */
    private $qualites;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Relevance", mappedBy="user", cascade={"persist", "remove"})
     */
    private $relevances;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\PostLike", mappedBy="user", cascade={"persist", "remove"})
     */
    private $likes;


    private $newPass;


    public function __construct()
    {
        parent::__construct();
        $this->news = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->relevances = new ArrayCollection();
        $this->qualites = new ArrayCollection();
        $this->likes = new ArrayCollection();
        // your own logic
    }


    public function getNewPass()
    {
        return $this->newPass;
    }

    /**
     * @param mixed $newPass
     * @return User
     */
    public function setNewPass($newPass)
    {
        $this->newPass = $newPass;
        return $this;
    }

    /**
     * @param News|ArrayCollection $news
     * @return User
     */
    public function addNews(News $news)
    {
        $this->news->add($news);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * @param ArrayCollection $comments
     * @return User
     */
    public function setComments(ArrayCollection $comments): User
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments(): ArrayCollection
    {
        return $this->comments;
    }

    /**
     * @param ArrayCollection $qualites
     * @return User
     */
    public function setQualites(ArrayCollection $qualites): User
    {
        $this->qualites = $qualites;
        return $this;
    }

    /**
     * @param ArrayCollection $relevances
     * @return User
     */
    public function setRelevances(ArrayCollection $relevances): User
    {
        $this->relevances = $relevances;
        return $this;
    }

    /**
     * @param ArrayCollection $likes
     * @return User
     */
    public function setLikes(ArrayCollection $likes): User
    {
        $this->likes = $likes;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getQualites(): ArrayCollection
    {
        return $this->qualites;
    }

    /**
     * @return ArrayCollection
     */
    public function getRelevances(): ArrayCollection
    {
        return $this->relevances;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikes(): ArrayCollection
    {
        return $this->likes;
    }


}