<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RelevanceRepository")
 */
class Relevance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $isGood;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\News", inversedBy="relevances")
     */
    private $news;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="relevances")
     */
    private $user;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $isGood
     * @return Relevance
     */
    public function setIsGood(int $isGood): Relevance
    {
        $this->isGood = $isGood;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * @return int
     */
    public function isGood(): int
    {
        return $this->isGood;
    }

    /**
     * @param mixed $news
     * @return Relevance
     */
    public function setNews($news)
    {
        $this->news = $news;
        return $this;
    }

    /**
     * @param mixed $user
     * @return Relevance
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
}
