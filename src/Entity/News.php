<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 */
class News
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", length=4096)
     * @var string
     */
    private $content;

    /**
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tags", inversedBy="news")
     */
    private $tags;


    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="news")
     */
    private $category;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $publishDate;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;


    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="news")
     */
    private $author;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Quality", mappedBy="news", cascade={"persist", "remove"})
     */
    private $qualites;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Relevance", mappedBy="news", cascade={"persist", "remove"})
     */
    private $relevances;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\PostLike", mappedBy="news", cascade={"persist", "remove"})
     */
    private $likes;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="news", cascade={"persist", "remove"})
     */
    private $comments;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->comments = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->relevances = new ArrayCollection();
        $this->qualites = new ArrayCollection();
        $this->likes = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param User $author
     * @return News
     */
    public function setAuthor(User $author): News
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @param string $content
     * @return News
     */
    public function setContent(string $content): News
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent():? string
    {
        return $this->content;
    }

    /**
     * @param mixed $tags
     * @return News
     */
    public function addTags($tags)
    {
        $this->tags->add($tags);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param \DateTime $publishDate |null
     * @return News
     */
    public function setPublishDate(?\DateTime $publishDate): News
    {
        $this->publishDate = $publishDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublishDate():? \DateTime
    {
        return $this->publishDate;
    }

    /**
     * @param \DateTime $createdAt
     * @return News
     */
    public function setCreatedAt(\DateTime $createdAt): News
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @param Category $category
     * @return News
     */
    public function setCategory(Category $category): News
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory():? Category
    {
        return $this->category;
    }

    /**
     * @param ArrayCollection $comments
     * @return News
     */
    public function addComments(ArrayCollection $comments): News
    {
        $this->comments->add($comments);
        return $this;
    }


    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param ArrayCollection $relevances
     * @return News
     */
    public function addRelevances(ArrayCollection $relevances): News
    {
        $this->relevances->add($relevances);
        return $this;
    }

    /**
     * @param ArrayCollection $likes
     * @return News
     */
    public function addLikes(ArrayCollection $likes): News
    {
        $this->likes->add($likes);
        return $this;
    }

    /**
     * @param ArrayCollection $qualites
     * @return News
     */
    public function addQualites(ArrayCollection $qualites): News
    {
        $this->qualites->add($qualites);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRelevances(): ArrayCollection
    {
        return $this->relevances;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikes(): ArrayCollection
    {
        return $this->likes;
    }

    /**
     * @param ArrayCollection $comments
     * @return News
     */
    public function setComments(ArrayCollection $comments): News
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @param mixed $tags
     * @return News
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function __toString()
    {
        return $this->getId() . "";
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }


}
