# language: ru

@news  @fixtures
Функционал: Тестируем комментарий

  @loginAdmin
  Сценарий: Добавление новой новости
    Допустим я нахожусь на главной странице
    И Я нажимаю на элемент "#app_create"
    И я пишу в поле wysiwyg "Содержание(контент)" значение "1qaz-wsx"
    И Я нажимаю на выпадающий список "#app_news_type_category" и выбираю "Мир"
    И я нажимаю на кнопку "app_news_type_save"
    Тогда я вижу слово "1qaz-wsx" на странице