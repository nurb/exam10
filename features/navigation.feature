# language: ru

@frontEnd  @loginAdmin @menu  @fixtures
Функционал: Тестируем меню навигации

  @frontEnd
  Структура сценария: Проход по разделам сайта по меню навигации SideBar
    Допустим Я нажимаю на элемент "<element_name>"
    И я вижу слово "<looking_word>" на странице

    Примеры:
      | element_name | looking_word |
      | #category-1  | Мир          |
      | #category-2  | Экономика    |
      | #category-3  | Спорт        |
      | #category-4  | Культура     |
      | #tags-1      | #добро       |
      | #tags-2      | #жизнь       |
      | #tags-3      | #люди        |
      | #tags-4      | #тэг         |
      | #app_profile | Профайл     |
