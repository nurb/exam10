# language: ru

@comment  @fixtures
Функционал: Тестируем создание новостей

  @loginAdmin
  Сценарий: Добавление новой новости
    Допустим я нахожусь на главной странице
    И Я нажимаю на элемент ".more-info"
    И я заполняю поля данными
      | app_comment_type_post | my-uniq-comment |
    И я нажимаю на кнопку "app_comment_type_save"
    Тогда я вижу слово "my-uniq-comment" на странице