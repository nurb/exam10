# language: ru
@auth  @fixtures
Функционал: Загрузите фикстуры перед началом теста. Тестируем регистрацию и авторизацию

  @register
  Сценарий: Я пытаюсь зарегистрироваться
    Допустим я нахожусь на главной странице
    И я вижу слово "Регистрация" на странице
    И я кликаю по ссылке с id "app_register"
    Тогда я заполняю поля данными
      | fos_user_registration_form_email                | test@gmail.com |
      | fos_user_registration_form_plainPassword_second | test123!@#     |
      | fos_user_registration_form_plainPassword_first  | test123!@#     |
      | fos_user_registration_form_username             | test           |
    И я нажимаю на кнопку "Зарегистрироваться"
    И я вижу слово "Поздравляем" на странице

  @register
  Сценарий: Я пытаюсь авторизоваться
    Допустим я нахожусь на главной странице
    И я кликаю по ссылке с id "app_login"
    Тогда я заполняю поля данными
      | username | morty  |
      | password | qwerty |
    И я нажимаю на кнопку "Войти"
    И я вижу слово "Выйти" на странице